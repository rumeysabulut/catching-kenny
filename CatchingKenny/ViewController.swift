//
//  ViewController.swift
//  CatchingKenny
//
//  Created by Rumeysa Bulut on 11.11.2019.
//  Copyright © 2019 Rumeysa Bulut. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var countDownLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var imageView3: UIImageView!
    @IBOutlet weak var imageView4: UIImageView!
    @IBOutlet weak var imageView5: UIImageView!
    @IBOutlet weak var imageView6: UIImageView!
    @IBOutlet weak var imageView7: UIImageView!
    @IBOutlet weak var imageView8: UIImageView!
    @IBOutlet weak var imageView9: UIImageView!
    
    @IBOutlet weak var highestScoreLabel: UILabel!
    
    var timer = Timer()
    var timerForChoosingImageView = Timer()
    var counter = 0   // time for game
    var score = 0
    var highestScore = 0
    var imgArray = [UIImageView]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        counter = 20
        countDownLabel.text = "\(counter)"
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(remainingTime), userInfo: nil, repeats: true)
        timerForChoosingImageView = Timer.scheduledTimer(timeInterval: 0.8, target: self, selector: #selector(chooseImageView), userInfo: nil, repeats: true)
        
        let storedScore = UserDefaults.standard.object(forKey: "highestScore")
        if let newScore = storedScore as? Int {
            highestScoreLabel.text = "High Score: \(newScore)"
            highestScore = newScore
        }
        else {
            highestScoreLabel.text = "High Score: 0"
        }
        
        imgArray = [imageView1, imageView2, imageView3, imageView4, imageView5, imageView6, imageView7, imageView8, imageView9]
        
        for img in imgArray {
            img.isUserInteractionEnabled = true
            img.isHidden = true  // A hidden view disappears from its window and does not receive input events.
        }
        
        let gestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(catchedKenny))
        let gestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(catchedKenny))
        let gestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(catchedKenny))
        let gestureRecognizer4 = UITapGestureRecognizer(target: self, action: #selector(catchedKenny))
        let gestureRecognizer5 = UITapGestureRecognizer(target: self, action: #selector(catchedKenny))
        let gestureRecognizer6 = UITapGestureRecognizer(target: self, action: #selector(catchedKenny))
        let gestureRecognizer7 = UITapGestureRecognizer(target: self, action: #selector(catchedKenny))
        let gestureRecognizer8 = UITapGestureRecognizer(target: self, action: #selector(catchedKenny))
        let gestureRecognizer9 = UITapGestureRecognizer(target: self, action: #selector(catchedKenny))
        
        imageView1.addGestureRecognizer(gestureRecognizer1)
        imageView2.addGestureRecognizer(gestureRecognizer2)
        imageView3.addGestureRecognizer(gestureRecognizer3)
        imageView4.addGestureRecognizer(gestureRecognizer4)
        imageView5.addGestureRecognizer(gestureRecognizer5)
        imageView6.addGestureRecognizer(gestureRecognizer6)
        imageView7.addGestureRecognizer(gestureRecognizer7)
        imageView8.addGestureRecognizer(gestureRecognizer8)
        imageView9.addGestureRecognizer(gestureRecognizer9)
        
        
    }
    
    @objc func remainingTime(){
        counter -= 1
        countDownLabel.text = "\(counter)"
        
        if counter == 0 {
            timer.invalidate()
            timerForChoosingImageView.invalidate()
            for img in imgArray {
                img.isHidden = true
            }
            setHighestScore()
            
            let alert = UIAlertController(title: "Time is over", message: "Do you want to play again?", preferredStyle: UIAlertController.Style.alert)
            let noButton = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil)
            let yesButton = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
                // Reset everything except the heighest score and start over
                self.score = 0
                self.scoreLabel.text = "Score: 0"
                self.counter = 20
                self.countDownLabel.text = "\(self.counter)"
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.remainingTime), userInfo: nil, repeats: true)
                self.timerForChoosingImageView = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.chooseImageView), userInfo: nil, repeats: true)
            }
            
            alert.addAction(noButton)
            alert.addAction(yesButton)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
   
    @objc func chooseImageView() {
        for img in imgArray {
            img.isHidden = true
        }
        let random = Int(arc4random_uniform(UInt32(imgArray.count)))
        imgArray[random].isHidden = false
    }
    
    
    @objc func catchedKenny(_ recognizer:UITapGestureRecognizer) {
        score += 1
        scoreLabel.text = "Score: \(score)"
    }
    
    func setHighestScore() {
        if score > highestScore {
            highestScore = score
            UserDefaults.standard.set(highestScore, forKey: "highestScore")
            highestScoreLabel.text = "High Score: \(highestScore)"
        }
    }
    
    @IBAction func clearHighScore(_ sender: Any) {
        highestScore = 0
        UserDefaults.standard.set(highestScore, forKey: "highestScore")
        highestScoreLabel.text = "High Score: \(highestScore)"
    
    }
    
    @IBAction func restartTheGame(_ sender: Any) {
        // Paused the game
        self.timer.invalidate()
        self.timerForChoosingImageView.invalidate()
        
        let pauseAlert = UIAlertController(title: "Paused", message: "Are you sure to restart?", preferredStyle: UIAlertController.Style.alert)
        // Resume the game
        let noButton = UIAlertAction(title: "Resume", style: UIAlertAction.Style.cancel) { (UIAlertAction) in
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.remainingTime), userInfo: nil, repeats: true)
            self.timerForChoosingImageView = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.chooseImageView), userInfo: nil, repeats: true)
        }
        // Reset everything except the heighest score and restart
        let yesButton = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.score = 0
            self.scoreLabel.text = "Score: 0"
            self.counter = 20
            self.countDownLabel.text = "\(self.counter)"
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.remainingTime), userInfo: nil, repeats: true)
            self.timerForChoosingImageView = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.chooseImageView), userInfo: nil, repeats: true)
        }
        
        pauseAlert.addAction(noButton)
        pauseAlert.addAction(yesButton)
        self.present(pauseAlert, animated: true, completion: nil)
    }
}
